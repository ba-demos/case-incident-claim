Incident claim case management
==============================================

The case simulate the management of incident claim in an insurance context.

Two stages:

1. First call: customer call and open the case
2. Investigation: expert engagement, doctor engagement, further customer call (many)

Two milestones:

- full statement: all information from the customer are collected
- agreement: expert reach an agreement about the incident costs

Case model
----------------------------------------------

![case](src/main/resources/com/demo/com.demo.incident-case-svg.svg)